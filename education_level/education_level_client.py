from db import get_jobs, get_connection, get_education_level
import requests
import re
import time
import sys
import pika
import json 
import os
#sys.path.insert(1, '/home/ajay/tutree_jobs_v2/search/squash_question_answer/')
from slack_notifier import slack_notifier

def callback(ch, method, properties, body):
    global global_db
    """ 
    RabbitMQ worker process stuff
    """
 
    print(" [x] Received %d bytes " % (len(body)))
    message = json.loads(body)
    if message['message'] is None or message['html_job_description_hash'] is None :
        #print(body)
        #print(" [x] Ignoring none record:%s " % (body))
        ch.basic_ack(delivery_tag=method.delivery_tag)
        return
 
 
    job_description = message['message'].lower()

    hash_id = message['html_job_description_hash']

    db = get_connection()

    start_time = time.time()
    
    try:
        result = requests.get('http://167.99.108.238:5080/', data = {'job_description': job_description})
    
        result_json=json.loads(result.text)
        
        education_id = result_json["education_level"]

        get_education_level(global_db, education_id, hash_id)

        ch.basic_ack(delivery_tag=method.delivery_tag)
        #this line return the current status of the queue
        response = ch.queue_declare(queue=queue_name, passive=True)
        
        #condition to check the the number of message in the queue
        #if the number of message count==0 
        if response.method.message_count==0:

            log_msg="Education level extraction| Step-3 of 3 | All jobs have been successfully processed"
            #send the log msg to the slack
            slack_notifier(log_msg, webhook_url)
            #exit from the program
            sys.exit()
        print("QA: Processed: %s in %s ms " % (hash_id,round(1000*(time.time() - start_time),2)))

    except Exception as e:
        print(e)

def main():
    """
    The purpose of this program, is to behave as a client, to both the mysql engine and the QA Engine.
    The program will put messages on a queue using mode=ENQUEUE which will later be consumed
    by one or more worker instances of the program consuming the program data using the mode=WORKER mode
    """
    global global_db
    global webhook_url
    global queue_name
    
    #slack API token
    webhook_url = os.getenv("SLACK_API_TOKEN")

    if len(sys.argv) <3:
        print("Usage: python3 qa_client.py <mode> <inq>")
        print("    mode:  [ENQUEUE|WORKER]")
        print("    inq:   RabbitMQ Queue Name")
        return

    #fetching params from run_in_dev.sh    
    mode = sys.argv[1]
    #queue_name = sys.argv[2]
    queue_name = "EDUCATION"

    #credentials
    credentials = pika.PlainCredentials(os.getenv("RMQ_USER"),os.getenv("RMQ_PASS"))
    parameters = pika.ConnectionParameters(host=os.getenv("RMQ_HOST"),port=int(os.getenv("RMQ_PORT")),virtual_host='/',credentials=credentials,heartbeat=0)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue_name, durable=True)

    #db connection
    global_db = get_connection()

    # enqueue the job_description data into rabbitMQ message queue
    if mode == "ENQUEUE":

        JOB_DESCRIPTION = 1
        HASH_id = 2

        db = get_connection()
        job_desc_data = get_jobs(db)
        #qc = {}
        #qc["TOTAL"] = len(d)
        x = 0
        total = len(job_desc_data)
        for job in job_desc_data:
            message = json.dumps({"message":job[JOB_DESCRIPTION], "html_job_description_hash":job[HASH_id]})
            channel.basic_publish(
                exchange='',
                routing_key=queue_name,
                body=message,
                properties=pika.BasicProperties(
                    delivery_mode=2,  # make message persistent
            ))
            print(" [x] Sent %s (%d)  [%d/%d] bytes" % (job[HASH_id], len(message), x, total))
            x += 1
        status_msg="Education level extraction| Step-1 of 3 | Enqueue: %s job descriptions have been added into queue '%s'"%(x,queue_name)
        #send the log msg to the slack
        slack_notifier(status_msg, webhook_url)
        connection.close() 

    #worker mode start consuming message 
    if mode == "WORKER":
        print(' [*] Waiting for messages. To exit press CTRL+C')
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(queue=queue_name, on_message_callback=callback)
        status_msg="Education level extraction| Step-2 of 3 | Worker: The client has started processing jobs from queue '%s'"%(queue_name)
        #send the log msg to the slack
        slack_notifier(status_msg, webhook_url)
        channel.start_consuming()

if __name__=="__main__":
    main()

    # job title.
    # https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/new_task.py
    # https://docs.python.org/3/library/json.html
