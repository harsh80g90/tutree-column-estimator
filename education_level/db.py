# process all jobs
import csv
#import mysql.connector # pip install mysql-connector-python
import os
import sqlalchemy
from sqlalchemy import create_engine
import pymysql
def get_connection():

    #get environment variable
    user=os.getenv("DBUSER")
    passwd=os.getenv("DBPASS")
    host=os.getenv("DBHOST")
    database=os.getenv("DBNAME")

    #conection string
    con_string="mysql+pymysql://{}:{}@{}/{}".format(user, passwd, host, database)

    #create engine
    sqlEngine = create_engine(con_string, pool_recycle=3600)
    db = sqlEngine.raw_connection()
    return db

def get_org_list():
    organization_file = open(os.getenv("organization_path"), "r")
    test_list = organization_file.readlines()
    organization_list = [x.strip() for x in test_list]

    organization_file.close()

    return organization_list

def get_jobs(db):
    db = get_connection()
    cur = db.cursor()

    organization_list = get_org_list()

    data = []
    organization_set = {'',}

    for organization in organization_list:
        query = 'select organization, html_job_description, html_job_description_hash from job where organization like "'+ organization +'%" and is_expired = 0 and education_level_id is NULL group by organization, html_job_description, html_job_description_hash'

        cur.execute(query)
        job_list = cur.fetchall()

        for row in job_list:
            data.append(row)
            organization_set.add(row[0])

    unique_org =tuple(organization_set)
    query = "select organization, html_job_description, html_job_description_hash from job where organization NOT IN {} and is_expired = 0 and education_level_id is NULL group by organization, html_job_description, html_job_description_hash". format(str(unique_org[1:]))
    cur.execute(query)
    job_list = cur.fetchall()
    for row in job_list:
        data.append(row)

    cur.close()
    db.close()

    return data
    

def get_education_level(db, education_id, hash_id):
    
    db = get_connection()
    mycursor = db.cursor()

    e_query = 'UPDATE job SET education_level_id = %s WHERE html_job_description_hash = %s' 
    VALUE = (education_id, hash_id)
    print(VALUE)
    mycursor.execute(e_query,VALUE)
    db.commit()
    mycursor.close()

if __name__ == "__main__":
    pass
    #main()
