# Education level extraction and prediction

## 1. Refer **predict_education_level_final.ipynb** file for data analysis and hyper parameter tuning.

## 2. Execute **predict_education_level.py** file to train education_level_prediction model.

## 3. **education_level_extraction.py** file contains api code to extract or predict education_level from job description.

## 4. **crontab_start.sh** file contains commands to add job_description into rabitmq, create new tmux session and process the records.