#!/bin/bash
##this script will be triggered from crontab and will start following program

#This command will add messages from db to rabbitMQ queue 
bash /home/harsh/tutree-column-estimator/education_level/add_to_queue.sh

#kill previous tmux session and start fresh session
tmux kill-ses -t education_level
tmux new-session -d -s education_level

#send keys into tmux window
tmux send-keys -t education_level:0 "bash /home/harsh/tutree-column-estimator/education_level/prod.sh" Enter
