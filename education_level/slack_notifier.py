import requests
import json

def slack_notifier(error_msg, webhook_url):
    
    #this block of code send the error msg to the slack channel if any error is occurred
    ## prepare the msg with error
    slack_data = {'text':error_msg}
    ##send the msg via http request
    response = requests.post(webhook_url, data=json.dumps(slack_data), headers={'Content-Type': 'application/json'})
    if response.status_code != 200:
        raise ValueError(
              'Request to slack returned an error %s, the response is:\n%s'
              % (response.status_code, response.text)
          )