CREATE TABLE IF NOT EXISTS education_level(
    id int AUTO_INCREMENT PRIMARY KEY,
    name varchar(100)
);


INSERT INTO education_level (name) VALUES ("High School Diploma or Equivalent"); 
INSERT INTO education_level (name) VALUES ("Bachelor’s degree");
INSERT INTO education_level (name) VALUES ("Master’s degree");

INSERT INTO education_level (name) VALUES ("Not found");
UPDATE education_level SET id = 0 WHERE name = "Not found";

ALTER TABLE job ADD education_level_id int;
ALTER TABLE job ADD CONSTRAINT FK_education_level_id FOREIGN KEY (education_level_id) REFERENCES education_level(id);