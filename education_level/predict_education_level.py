# purpose of this code is to predict education_level for every job descrption
# prediction model is trained on the labeled data of job description and their education level.

import pandas as pd
from tqdm import tqdm
import re
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import nltk
from nltk import word_tokenize 
from nltk.util import ngrams
from nltk.corpus import stopwords
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score, confusion_matrix
import joblib
nltk.download('punkt')
nltk.download('stopwords')

def clean_text(text):
    
    """
        text: a string
        return: modified initial string
    """
    
    cleaner = re.compile('<.*?>')
    REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
    BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
    STOPWORDS = set(stopwords.words('english'))

    text = re.sub(cleaner, ' ', text)
    text = text.lower() # lowercase text
    text = REPLACE_BY_SPACE_RE.sub(' ', text) # replace REPLACE_BY_SPACE_RE symbols by space in text
    text = BAD_SYMBOLS_RE.sub('', text) # delete symbols which are in BAD_SYMBOLS_RE from text
    text = ' '.join(word for word in text.split() if word not in STOPWORDS) # delete stopwords
    return text

def get_data():

    """
       Description: get_data imports pre-classified data set, the data set contains 
                    Job titles, Job descriptions and education_level.

       Pre_processing: The imported dataset goes through pre-processing,
                       pre-processing contains,
                       - De duplication.
                       - Combining job titles and job description.
    """
    
    df = pd.read_csv('jobs_data_with_title.csv')
    df.columns=['', 'title', 'description', 'category']

    df = df.drop_duplicates(subset =["title", "description"])

    df.reset_index(inplace = True, drop = True)

    # Combining job_title and job description
    df['combine_text']=df['title']+' '+df['description']

    # Apply text cleaning
    df['combine_text'] = df['combine_text'].apply(clean_text)

    return df

def get_avg_word_to_vec(text_column):

    Xtrain_job_avg_w2v_vectors = []; 
    for sentence in text_column:
        vector = np.zeros(300)
        cnt_words =0
        for word in sentence.split():
            if word in glove_words:
                vector += model[word]
                cnt_words += 1
        if cnt_words != 0:
            vector /= cnt_words
        Xtrain_job_avg_w2v_vectors.append(vector)

    return Xtrain_job_avg_w2v_vectors

def get_word_vec(text_column):

    Xtrain_job_avg_w2v_vectors = []

    for sentence in text_column:
        # initialize array of vector
        vector = np.zeros(300)

        # check if the word is present in glove vectors keys or not
        for word in sentence.split():
            if word in glove_words:
                vector += model[word]

        Xtrain_job_avg_w2v_vectors.append(vector)

    return Xtrain_job_avg_w2v_vectors

def main():
    # Get pre-classified data for model training.
    # main data contains job titles, job descriptions and education_level.
    main_data = get_data()

    print("No. of rows in Data set = ",main_data.shape[0])
    print("No. of columns in Data set = ",main_data.shape[1])
    print("--------------------------------------------------")

    # spliting text data and education_level
    X = main_data.combine_text
    Y = main_data.category
    
    # Encoding and model training.
    # Value of alpha is decided after hyper parameter tuning.
    x_main = get_word_vec(X['combine_text'])

    Classifier = SGDClassifier(alpha=0.1, loss='log', class_weight='balanced', penalty='l2', random_state=42)
    Classifier.fit(x_main, y)
    
    # save the model to disk
    filename = 'education_level_prediction_model.sav'
    joblib.dump(Classifier, filename)


if __name__=="__main__":
    main():