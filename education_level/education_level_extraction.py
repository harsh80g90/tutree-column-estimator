#This code is responsible for extracting education level from job description.

import re
import os
from flask import Flask, request, jsonify
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
import nltk
from nltk import word_tokenize 
from nltk.util import ngrams
from nltk.corpus import stopwords
from sklearn.metrics.pairwise import cosine_similarity
import joblib
# nltk.download('punkt')
# nltk.download('stopwords')

def clean_data(job_data):

    #removes html tags and symbols.
    cleaner = re.compile('<.*?>')
    txt = re.sub(cleaner, ' ', job_data)
    result = txt.lower()

    #remove special characters
    result_2 = re.sub("[^/a-zA-Z,.&:]",' ', result)

    #removes multiple spaces
    new_result = re.sub(' +', ' ',result_2)
    return new_result

def deep_clean(text_data):
    
    #remove special characters
    text = re.sub("[^a-zA-Z]",' ', text_data)
    STOPWORDS = set(stopwords.words('english'))
    text = ' '.join(word for word in text.split() if word not in STOPWORDS) # delete stopwords from text

    return text

def get_avg_vec(text, glove_words):
    vectors_list = []; 
    for sentence in text:
        vector = np.zeros(300)
        cnt_words = 0
        for word in sentence.split(' '):
            if word in glove_words:
                vector += model[word]
                cnt_words += 1
        if cnt_words != 0:
            vector /= cnt_words
        vectors_list.append(vector)

    return vectors_list

def get_education_category(raw_result):

    category_result = 0
    all_category = []

    # qualification_dict contains related words for high school, bachelor's and master's degree 
    qualification_dict={
                    1 : ['high school', 'senior high', 'senior secondary school', 'secondary school', 
                        'high school diploma', 'college preparatory school', 'intermediate school', 'intermediate school', 
                            'junior high school', 'middle school', 'senior high school', 'eleventh grade', 'secondary school', 
                            'tenth grade', 'common school', 'elementary school', 'grammar school', 'public school', 'trade school', 
                            'training school', 'academe', 'academy', 'seminary', 'boarding school', 'charter school', 'higher education'],
                    
                    2 : ['bachelor s', 'bachelors degree', 'graduate', 'graduation', 'btech', 'ba', 'bs', 'undergraduate', 'undergraduation', 
                            'under-graduate', 'under-graduation', 'baccalaureate', 'bachelor of Science', 'associate\'s degree', 'bed', 'bsc', 
                            'chartered', 'academy degree', 'fellowship', 'first class', 'graduation honorary', 'university', 'graduate degree', 
                            'alumnus', 'grad', 'degree holder', 'conferring of degrees', 'graduation exercise', 'scholarly', 'scholastic', 'collegiate'],
                    
                    3 : ['master s degree', 'master s', 'postgraduation', 'post graduate', 'post graduation', 'ms', 'associate degree', 'advanced degree', 
                            'doctorate', 'postgrad degree', 'phd']
                    }

    for dict_key in qualification_dict.keys():
        for word in qualification_dict[dict_key]:
            if (str(raw_result).find(word) != -1):
                all_category.append(dict_key)

    if len(all_category):
        category_result = all_category[0]

    return category_result

def result_by_similarity(text, glove_words):
    
    # full_category string is used to create a vector to detect sentences having similar words for education level

    # the purpose of this regular expression is to split job description into sentences
    test_string_list = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', text)
    test_list_vector = [get_avg_vec([deep_clean(i)], glove_words) for i in test_string_list]

    focus_sentence = []
    similarity_score = []
    final_sentence = []
    score = []

    # sentence with the highest score of similarity will be return as a final sentence 
    for index, i in enumerate(test_list_vector):
        full_vector = cosine_similarity(full_category_vector, i)

        if full_vector >= 0.8:
            focus_sentence.append(test_string_list[index])
            similarity_score.append(full_vector)

    sentence_score = dict(zip(focus_sentence, similarity_score))

    if len(similarity_score):
        final_sentence = max(sentence_score, key=sentence_score.get)
        score = max(similarity_score)

    return final_sentence, score

def get_predicted_education_level(text):
    
    # encode the data point
    text_vector = get_avg_vec([text], glove_words)
    
    # predict education_level
    raw_result = loaded_model.predict(np.array(text_vector))
    result = int(raw_result[0])

    return result

def get_result(job):

    final_category = 0

    # basic cleaning
    clean_text = clean_data(job)
    #print(clean_text)

    # result contains sentence with education level and its similarity score
    result, score = result_by_similarity(clean_text, glove_words)

    final_category = get_education_category(result)

    if final_category == 0:

        # perform prediction where extraction fails 
        final_category = get_predicted_education_level(clean_text)

    return final_category

def load_global_parameters():

    global model
    global glove_words
    global full_category_vector

    with open('glove_vectors', 'rb') as f:

       model = pickle.load(f)
       glove_words = set(model.keys())

    full_category = ["""requirement education requirements education/experience education requirements education/qualifications qualifications
                    high school senior high senior secondary school secondary school high school diploma college preparatory 
                    school intermediate school intermediate school junior high school middle school senior high school eleventh 
                    grade secondary school tenth grade common school elementary school grammar school public school trade school 
                    training school academe academy seminary boarding school charter school higher education bachelor s bachelors 
                    degree graduate graduation btech ba bs undergraduate undergraduation under-graduate under-graduation baccalaureate 
                    bachelor of Science associate\'s degree bed bsc chartered academy degree fellowship first class graduation honorary 
                    university graduate degree alumnus grad degree holder conferring of degrees graduation exercise scholarly scholastic 
                    collegiate master s degree master s postgraduation post graduate post graduation ms associate degree advanced degree 
                    doctorate postgrad degree phd"""]

    full_category_vector = get_avg_vec(full_category, glove_words)

    return

app = Flask(__name__)

@app.route('/',methods=['POST','GET'])
def education_level():

    job_description = request.form.get("job_description")

    # get category id
    category_id = get_result(job_description)

    result_dict = {"education_level" : category_id }

    return jsonify(result_dict)

if __name__ == "__main__":

    global loaded_model
    
    # Load the model from disk
    model = os.getenv("education_prediction_prediction_model")
    loaded_model = joblib.load(model)
    
    load_global_parameters()
    
    app.run(host="167.99.108.238", port=5080)
    #app.run(port=5080)