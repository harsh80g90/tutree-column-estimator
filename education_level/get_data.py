import pandas as pd 
# process all jobs
import csv
#import mysql.connector # pip install mysql-connector-python
import os
import sqlalchemy
from sqlalchemy import create_engine
import pymysql


def get_connection():

    #get environment variable
    user=os.getenv("DBUSER")
    passwd=os.getenv("DBPASS")
    host=os.getenv("DBHOST")
    database=os.getenv("DBNAME")

    #conection string
    con_string="mysql+pymysql://{}:{}@{}/{}".format(user, passwd, host, database)

    #create engine
    sqlEngine = create_engine(con_string, pool_recycle=3600)
    db = sqlEngine.raw_connection()
    return db


def get_jobs(query):
    db = get_connection()
    cur = db.cursor()
    
    cur.execute(query)
    job_list = cur.fetchall()

    this_one = pd.DataFrame(list(job_list),columns=['description','id'])    
    
    cur.close()
    db.close()

    return this_one

def main():

    query_1 = 'select ANY_VALUE(html_job_description), ANY_VALUE(education_level_id) from job where is_expired = 0 and education_level_id = 0 group by html_job_description_hash limit 3000'
    query_2 = 'select ANY_VALUE(html_job_description), ANY_VALUE(education_level_id) from job where is_expired = 0 and education_level_id = 1 group by html_job_description_hash'
    query_3 = 'select ANY_VALUE(html_job_description), ANY_VALUE(education_level_id) from job where is_expired = 0 and education_level_id = 2 group by html_job_description_hash'
    query_4 = 'select ANY_VALUE(html_job_description), ANY_VALUE(education_level_id) from job where is_expired = 0 and education_level_id = 3 group by html_job_description_hash'


    query_list = [query_1,query_2,query_3,query_4]

    main_data = pd.DataFrame() 
    for i in query_list:
        sub_data = get_jobs(i)

        main_data = main_data.append(sub_data, ignore_index = True)
    
    print(main_data.shape)
    main_data.to_csv("jobs_data.csv")

    return


if __name__ == "__main__":
    main()
